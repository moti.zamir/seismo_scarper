import csv
from datetime import date
from lxml import html
import requests
import sys
import os
from random import randint
from time import sleep

os.system('clear')
domain = str(sys.argv[1])
pages = int(sys.argv[2])
the_date = date.today()
cur_date = the_date.strftime("%d-%m-%Y")
raw_csv_file_name = domain + '-raw-' + cur_date + '.csv'
output_file = f"raw/{raw_csv_file_name}"

HEADERS = {
    'authority': 'www.linkedin.com',
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"',
    'sec-ch-ua-mobile': '?0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'en-US,en;q=0.9',
}

URL_SPLIT = '&location=Israel&geoId=101620260&trk=public_jobs_jobs-search-bar_search-submit&redirect=false&position=1&pageNum=0&sortBy=DD'


def domain_picker(the_domain):
    domains = {
        "backend": '%28backend%20OR%20Back-End%20OR%20%22Back%20End%22%29%20NOT%20DevOps',
        "data": '%28%22data%22%20OR%20bigdata%20OR%20%22big-data%22%29%20NOT%20DevOps%20AND%20NOT%20%22Back%20End%22%20AND%20NOT%20%22Front%20End%22',
        "ml": '%28mlops%20OR%20ml-ops%29%20NOT%20DevOps',
        "frontend": '%28frontend%20OR%20Front-End%20OR%20%22Front%20End%22%29%20NOT%20DevOps',
        "fullstack": "%28fullstack%20OR%20Full-Stack%20OR%20%22Full%20Stack%22%29%20NOT%20DevOps",
        "devops": "Devops%20OR%20SRE%20%20OR%20secops",
        "mobile": 'Mobile%20OR%20Android%20OR%20Flutter%20OR%20%22React%20Native%22%20NOT%20ios'
    }
    return domains.get(the_domain, "")


def scrape_it(domain, pages):
    output_file = f"raw/{raw_csv_file_name}"
    with open(output_file, 'w', encoding='utf-8') as raw_file:
        fields = [
            'JobID',
            'Date',
            'CompanyName',
            'JobTitle',
            'Description'
        ]

        writer = csv.DictWriter(raw_file, fieldnames=fields)
        writer.writeheader()

        for page in range(0, pages):
            print("\n\nSTARTING: " + domain + " | PAGE NUM: " + str(page) + "\n\n")
            listing_url = f'https://www.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/search?keywords={domain}{URL_SPLIT}&start={page * 25}'
            data = parse_jobs_listing_page(listing_url)
            writer.writerows(data)

    print('\n\nDONE RAW', output_file)
    # os.system(f'python3 anz.py {domain}')
    # print('\n\nDONE ANALYZING', output_file)


def parse_jobs_listing_page(jobs_listing_url):
    sleep(randint(1, 3))
    jobs_on_page = []
    response = requests.get(jobs_listing_url, headers=HEADERS)
    tree = html.fromstring(response.content)

    all_jobs = tree.xpath('//li/div')

    job_count = 1

    for job in all_jobs:
        job_object = {}
        try:
            job_id = job.xpath('.//@data-entity-urn')
            company_name = job.xpath('.//h4/a/text()')
            job_title = job.xpath('.//h3/text()')
            date_ = job.xpath('.//time/@datetime')
            job_link = job.xpath('./a/@href')[0]

            while True:
                print(
                    f'[{job_count}] COLLECTING DATA FROM JOB(ID): {job_id[0].replace("urn:li:jobPosting:", "").strip()} ... ')
                job_count += 1
                sleep(randint(1, 3))
                response = requests.get(job_link, headers=HEADERS)
                if response.status_code != 429:
                    break
            tree = html.fromstring(response.content)

            job_description_raw = tree.xpath('//div[contains(@class,"text description")]//text()')
            job_description = (''.join(job_description_raw)).replace('Show less', '').replace('Show more', '')

            job_object.update({
                'JobID': job_id[0].replace("urn:li:jobPosting:", "").strip() if job_id else None,
                'Date': date_[0].strip() if date_ else None,
                'CompanyName': company_name[0].strip() if company_name else None,
                'JobTitle': job_title[0].strip() if job_title else None,
                'Description': job_description.strip() if job_description else None
            })
        except:
            pass
        if job_object.get('JobID'):
            jobs_on_page.append(job_object)

    return jobs_on_page


if __name__ == '__main__':
    domain = str(sys.argv[1])
    pages = int(sys.argv[2])

    scrape_it(domain, pages)
