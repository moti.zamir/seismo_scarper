import asyncio
import json
import os
import pickle
from datetime import date
import pandas as pd
from src import constants
from src import jobs_parser
import logging
from src import config
from io import StringIO
from kafka import KafkaProducer

logger = logging.getLogger(__name__)


async def task_in_line(pages, delay):
    while True:
        logger.info('Starting ')
        await start_scarping(pages)
        logger.info("Finished task")
        await asyncio.sleep(delay)


def prepare_output_file(domain):
    today = date.today()
    current_date = today.strftime("%d-%m-%Y")
    raw_csv_file_name = f"{domain}-raw-{current_date}.csv"
    return f'{constants.OUTPUT_DIR}{raw_csv_file_name}'


async def start_scarping(pages):
    s3 = config.session.client('s3')
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda x:
                             json.dumps(x).encode('utf-8'), api_version=(2, 5, 0))
    logger.info("Starting scraping")
    with open(os.path.join(os.path.dirname(__file__), 'domains')) as f:
        domains = f.read().splitlines()

    for domain in domains:
        listing_url = f"{constants.LIST_URL}{domain}{constants.URL_SPLITS}&start="
        output_file = prepare_output_file(domain)
        with open(os.path.join(os.path.dirname(__file__), 'fields.pic'), 'rb') as f:
            fields = pickle.load(f)
        data_list = []

        for page in range(0, pages):
            logger.info(f"STARTING: {domain} PAGE NUM: {page}")
            data = jobs_parser.parse_jobs_listing_page(f"{listing_url}{page*25}")
            for item in data:
                data_list.append(item)
                producer.send(topic=domain, value=item)
                print('\n\nPRODUCE SUCCESS\n\n')
        df = pd.DataFrame(data=data_list, columns=fields)
        csv_buffer = StringIO()
        df.to_csv(csv_buffer)

        s3.put_object(Body=csv_buffer.getvalue(), Bucket=config.conf('BUCKET_NAME'), Key=output_file)


def stop_scraping():
    pass
