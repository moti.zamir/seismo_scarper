import os

DAILY = int(os.getenv('DAILY_CYCLE', '86400'))
WEEKLY = int(os.getenv('WEEKLY_CYCLE', '604800'))
# PAGES = int(os.getenv('PAGES', '5'))
PAGES = int(os.getenv('PAGES', '1'))

# LIST_OF_FREQS = os.getenv("LIST_OF_FREQS", "86400,604800")
LIST_OF_FREQS = os.getenv("LIST_OF_FREQS", "10,15")

OUTPUT_DIR = os.getenv('OUTPUT_DIR', 'raw/')

URL_SPLITS = os.getenv('URL_SPLITS', '&location=Israel&geoId=101620260&trk=public_jobs_jobs-search-bar_search-submit'
                                     '&redirect=false&position=1&pageNum=0&sortBy=DD')

LIST_URL = os.getenv('LIST_URL', 'https://www.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/search?keywords=')

