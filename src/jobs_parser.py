from time import sleep
from lxml import html
from src import config
import requests
from random import randint
import logging

logger = logging.getLogger(__name__)


def parse_jobs_listing_page(jobs_listing_url):
    headers = config.headers
    sleep(randint(1, 3))
    jobs_on_page = []
    response = requests.get(jobs_listing_url, headers=headers)
    tree = html.fromstring(response.content)

    all_jobs = tree.xpath('//li/div')

    job_count = 1

    for job in all_jobs:
        job_object = {}
        try:
            job_id = job.xpath('.//@data-entity-urn')
            company_name = job.xpath('.//h4/a/text()')
            job_title = job.xpath('.//h3/text()')
            date_ = job.xpath('.//time/@datetime')
            job_link = job.xpath('./a/@href')[0]

            while True:
                logger.info(f'[{job_count}] COLLECTING DATA FROM JOB(ID): {job_id[0].replace("urn:li:jobPosting:", "").strip()} ... ')
                job_count += 1
                sleep(randint(1, 3))
                response = requests.get(job_link, headers=headers)
                if response.status_code != 429:
                    break
            tree = html.fromstring(response.content)

            job_description_raw = tree.xpath('//div[contains(@class,"text description")]//text()')
            job_description = (''.join(job_description_raw)).replace('Show less', '').replace('Show more', '')

            job_object.update({
                'JobID': job_id[0].replace("urn:li:jobPosting:", "").strip() if job_id else None,
                'Date': date_[0].strip() if date_ else None,
                'CompanyName': company_name[0].strip() if company_name else None,
                'JobTitle': job_title[0].strip() if job_title else None,
                'Description': job_description.strip() if job_description else None
            })
        except Exception as e:
            logger.error(e)
        if job_object.get('JobID'):
            jobs_on_page.append(job_object)

    return jobs_on_page
