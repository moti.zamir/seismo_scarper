import pytest
from mock import patch
from src.scarper_service import prepare_output_file


@patch('src.scraper_service.date.today', return_value='1782-07-18')
def test_prepare_output_file():
    domain = 'backend'
    assert prepare_output_file(domain) == '/raw/backend-raw-1782-07-18.csv'
