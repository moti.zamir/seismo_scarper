FROM python:3.8.13-slim
RUN apt-get update && apt-get install -y --no-install-recommends \
    libpq-dev build-essential && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

EXPOSE 6500

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "6500"]