import asyncio
from fastapi import FastAPI
from src import app_logger, constants, scarper_service, config

app_logger.create_logger()
app = FastAPI()


@app.on_event('startup')
async def start_scraping():
    for item in config.conf('LIST_OF_FREQS').split(','):
        asyncio.create_task(scarper_service.task_in_line(constants.PAGES, int(item)))


@app.on_event("shutdown")
def shutdown_event():
    with open("src/log.txt", mode="a") as log:
        log.write("Application shutdown")


@app.post('/scraping/stop', status_code=200)
async def stop_scraping():
    scarper_service.stop_scraping()
